//
//  ViewController.swift
//  trabalhopersonagem
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct Personagem: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController,UITableViewDataSource {
    var listaPersonagens:[Personagem]?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaPersonagens?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MinhaCelula
        let personagem = listaPersonagens![indexPath.row]
        
        celula.texto1.text = personagem.name
        celula.texto2.text = personagem.actor
        celula.imagempersonagem.kf.setImage(with: URL(string: personagem.image))
        return celula
    }
    
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        url()
        tableview.dataSource = self
    }
    private func url(){
    AF.request("https://hp-api.herokuapp.com/api/characters")
        .responseDecodable(of: [Personagem].self){
        response in
            if let actor_list = response.value{
            self.listaPersonagens = actor_list
            }
            self.tableview.reloadData()
        }
    }
}

